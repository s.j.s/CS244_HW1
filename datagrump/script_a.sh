#!/bin/bash
for new_window_size in {10..150..5}
do
new_line="   unsigned int the_window_size=$new_window_size ;"
sed -i "/unsigned int the_window_size/c\ $new_line" controller.cc
cd ..
./autogen.sh && ./configure && make
cd datagrump
skip_line=$'\n'
exec 2>>output_a.txt
echo "${skip_line}window size=$new_window_size${skip_line}">>output_a.txt
./run-contest enternamehere
done
exit 0
